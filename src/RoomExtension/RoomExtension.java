package RoomExtension;

import java.sql.SQLException;
import java.util.List;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;
import com.smartfoxserver.v2.extensions.SFSExtension;

import PokerEngine.Card;
import PokerEngine.Player;
import PokerEngine.Table;

public class RoomExtension extends SFSExtension {

	private static final int BLIND_VALUE = 10;
	private static final int STARTING_CASH = 500;

	public Table table;
	public String tableName = "";
	public int blind = 10;
	public int buyin = 500;
	public int size = 5;

	@Override
	public void init() {
		addRequestHandler("ready", ReadyHandler.class);
		addRequestHandler("join", JoinHandler.class);
		addRequestHandler("bid", BidHandler.class);
		addRequestHandler("arrange", ArrangeHandler.class);
		addRequestHandler("exitRoom", ExitRoomHandler.class);
		addRequestHandler("tip", TipHandler.class);
		addRequestHandler("switchCard", SwitchCardHandler.class);

//		ISFSObject roomData = this.getParentRoom().getVariable("param").getSFSObjectValue();
//		tableName = roomData.getUtfString("table_name");
//		blind = roomData.getInt("blind");
//		buyin = roomData.getInt("buyin");
//		size = roomData.getInt("size");
//		System.out.println(tableName + blind + buyin + size);
		
		tableName = "Room 1";
		blind = 10;
		buyin = 500;
		size = 5;

		table = new Table(blind, this);
		
		table.addPlayer(new Player("Mike", "Mike", 400, true));
		table.addPlayer(new Player("Jerry", "Jerry", 300, true));
		table.addPlayer(new Player("Tom", "Tom", 500, true));

	}

	@Override
	public void destroy() {
		super.destroy();
	}

	@Override
	public Object handleInternalMessage(String cmdName, Object params) {
		if (cmdName.equals("getRoomInfo")) {
			ISFSObject obj = new SFSObject();
			obj.putUtfString("room_id", getParentRoom().getName());
			obj.putUtfString("table_name", tableName);
			obj.putInt("blind", blind);
			obj.putInt("buyin", buyin);
			obj.putInt("size", size);
			obj.putInt("player_num", table.playerSize());
			return obj;
		} else if (cmdName.equals("getBotList")) {
			ISFSArray array = new SFSArray();
			for (Player player : table.players) {
				if (player.isBot()) {
					ISFSObject obj = new SFSObject();
					obj.putUtfString("room_id", getParentRoom().getName());
					obj.putUtfString("table_name", tableName);
					obj.putUtfString("name", player.getName());
					obj.putInt("chip", player.getCash());
					obj.putInt("level", player.level);
					array.addSFSObject(obj);
				}
			}
			for (Player player : table.newPlayers) {
				if (player.isBot()) {
					ISFSObject obj = new SFSObject();
					obj.putUtfString("room_id", getParentRoom().getName());
					obj.putUtfString("table_name", tableName);
					obj.putUtfString("name", player.getName());
					obj.putInt("chip", player.getCash());
					obj.putInt("level", player.level);
					array.addSFSObject(obj);
				}
			}
			return array;
		} else if (cmdName.equals("addBot")) {
			ISFSObject obj = (ISFSObject) params;
			ISFSObject response = new SFSObject();
			if (table.playerSize() == size) {
				response.putBool("success", false);
				response.putUtfString("error", "size");
				return response;
			} else {
				for (Player player : table.players) {
					if (player.isBot()) {
						if (player.getName().compareTo(obj.getUtfString("name")) == 0) {
							response.putBool("success", false);
							response.putUtfString("error", "name");
							return response;
						}
					}
				}
				for (Player player : table.newPlayers) {
					if (player.isBot()) {
						if (player.getName().compareTo(obj.getUtfString("name")) == 0) {
							response.putBool("success", false);
							response.putUtfString("error", "name");
							return response;
						}
					}
				}
				Player player = new Player(obj.getUtfString("name"), obj.getUtfString("name"), obj.getInt("chip"),
						true);
				player.level = obj.getInt("level");
				table.addPlayer(player);
				showPlayers();
				if (!table.isRunning()) {
					if (table.playerSize() >= 2)
						table.run();
				} else {
					showNewPlayer();
				}
				List<User> userList = (List<User>)getParentZone().getUserList();
				if(userList != null)
					send("userCountChange", new SFSObject(), userList);
				response.putBool("success", true);
				return response;
			}
		} else if (cmdName.equals("removeBot")) {
			ISFSObject obj = (ISFSObject) params;
			for (Player player : table.players) {
				if (player.isBot()) {
					if (player.getName().compareTo(obj.getUtfString("name")) == 0) {
						table.players.remove(player);
						List<User> userList = (List<User>)getParentZone().getUserList();
						if(userList != null)
							send("userCountChange", new SFSObject(), userList);
						return null;
					}
				}
			}
			for (Player player : table.newPlayers) {
				if (player.isBot()) {
					if (player.getName().compareTo(obj.getUtfString("name")) == 0) {
						table.newPlayers.remove(player);
						List<User> userList = (List<User>)getParentZone().getUserList();
						if(userList != null)
							send("userCountChange", new SFSObject(), userList);
						return null;
					}
				}
			}
		}
		return null;
	}

	public void readyPlayer(ISFSObject obj) {
		String username = obj.getUtfString("username");
		boolean flag = false;
		for (Player player : table.players) {
			if (username.compareTo(player.getUsername()) == 0)
				flag = true;
		}
		for (Player player : table.newPlayers) {
			if (username.compareTo(player.getUsername()) == 0)
				flag = true;
		}
		ISFSObject resObj = new SFSObject();
		resObj.putBool("alreadyJoin", flag);
		if (getParentRoom().getUserByName(username) != null)
			send("ready", resObj, getParentRoom().getUserByName(username));

		showPlayers();
		showNewPlayer();
	}

	public void joinNewPlayer(ISFSObject obj) {
		if (table.playerSize() == size)
			return;

		String username = obj.getUtfString("username");
		String name = obj.getUtfString("name");
		int buy_in = obj.getInt("buyin");

		System.out.println(username + "," + name);

		for (Player player : table.players) {
			if (username.compareTo(player.getUsername()) == 0) {
				showPlayers();
				showNewPlayer();
				return;
			}
		}
		for (Player player : table.newPlayers) {
			if (username.compareTo(player.getUsername()) == 0) {
				showPlayers();
				showNewPlayer();
				return;
			}
		}

		Player player = new Player(username, name, buy_in, false);
		table.addPlayer(player);
		payCash(username, buy_in);
		showPlayers();
		if (!table.isRunning()) {
			if (table.playerSize() >= 2)
				table.run();
		} else {
			showNewPlayer();
		}
		List<User> userList = (List<User>)getParentZone().getUserList();
		if(userList != null)
			send("userCountChange", new SFSObject(), userList);
	}

	public void exitPlayer(ISFSObject obj) {
		String username = obj.getUtfString("username");
		String name = obj.getUtfString("name");
		
		System.out.println(username);

		for (Player player : table.players) {
			if (username.compareTo(player.getUsername()) == 0) {
				if (!table.isRunning()) {
					table.players.remove(player);
					removePlayer(player);
				} else {
					table.exitPlayers.add(player);
					player.isExit = true;
				}
				return;
			}
		}
		for (Player player : table.newPlayers) {
			if (username.compareTo(player.getUsername()) == 0) {
				table.newPlayers.remove(player);
				removePlayer(player);
				return;
			}
		}
	}

	public void removePlayer(Player player) {
		addCash(player.getUsername(), player.getCash());
		User user = getParentZone().getUserByName(player.getUsername());
		if (user != null)
			send("leaveRoom", new SFSObject(), user);
		List<User> userList = (List<User>)getParentZone().getUserList();
		if(userList != null)
			send("userCountChange", new SFSObject(), userList);
		showPlayers();
	}

	public void tipPlayer(String username) {
		for (Player player : table.players) {
			if (username.compareTo(player.getUsername()) == 0) {
				int chip = player.getCash();
				if (chip < 50) {
					player.payCash(chip);
					addCash("root", chip);
					addTopup(username, -chip);
				} else {
					player.payCash(50);
					addCash("root", 50);
					addTopup(username, -50);
				}
				showPlayers();
				return;
			}
		}
		for (Player player : table.newPlayers) {
			if (username.compareTo(player.getUsername()) == 0) {
				int chip = player.getCash();
				if (chip < 50) {
					player.payCash(chip);
					addCash("root", chip);
					addTopup(username, -chip);
				} else {
					player.payCash(50);
					addCash("root", 50);
					addTopup(username, -50);
				}
				showPlayers();
				return;
			}
		}
	}

	public void switchCard(String username) {
		for (Player player : table.players) {
			if (username.compareTo(player.getUsername()) == 0) {
				table.switchCard(player);
				player.payCash(50);
				addCash("root", 50);
				addTopup(username, -50);
				showPlayers();
				dealSecondCards(true);
				return;
			}
		}
		for (Player player : table.newPlayers) {
			if (username.compareTo(player.getUsername()) == 0) {
				table.switchCard(player);
				player.payCash(50);
				addCash("root", 50);
				addTopup(username, -50);
				showPlayers();
				dealSecondCards(true);
				return;
			}
		}
	}

	public void showPlayers() {
		if (getParentRoom().getUserList() != null)
			send("resetPlayers", new SFSObject(), getParentRoom().getUserList());
		for (Player player : table.players) {
			showPlayer(player, false);
		}
		for (Player player : table.newPlayers) {
			showPlayer(player, true);
		}
	}

	public void setTimer() {
		ISFSObject obj = new SFSObject();
		long curTime = System.currentTimeMillis();
		float value = 60f - (curTime - table.startTime) * 0.001f;
		obj.putFloat("value", value);
		if (getParentRoom().getUserList() != null)
			send("setTimer", obj, getParentRoom().getUserList());
	}

	public void showPlayer(Player player, boolean isNew) {
		ISFSObject obj = new SFSObject();
		obj.putUtfString("username", player.getUsername());
		obj.putUtfString("name", player.getName());
		obj.putInt("cash", player.getCash());
		obj.putBool("isNew", isNew);
		obj.putBool("isBusted", player.isBusted);
		if (getParentRoom().getUserList() != null)
			send("showPlayer", obj, getParentRoom().getUserList());
	}

	public void resetHand() {
		if (getParentRoom().getUserList() != null)
			send("resetHand", new SFSObject(), getParentRoom().getUserList());
	}

	public void dealFirstCards(boolean isNew) {
		for (Player player : table.players) {
			if (player.isBusted)
				continue;
			dealFirstCards(player, isNew);
		}
	}

	public void dealFirstCards(Player player, boolean isNew) {
		ISFSObject obj = new SFSObject();
		obj.putUtfString("username", player.getUsername());
		obj.putBool("isNew", isNew);
		Card[] cards = player.getCards();
		for (int i = 0; i < 3; i++)
			obj.putInt("card" + i, cards[i].hashCode());
		if (getParentRoom().getUserList() != null)
			send("dealFirstCards", obj, getParentRoom().getUserList());
	}

	public void sendBid() {
		for (Player player : table.players) {
			if (player.isBusted)
				continue;
			sendBid(player);
		}
	}

	public void sendBid(Player player) {
		if (getParentRoom().getUserByName(player.getUsername()) != null)
			send("bid", new SFSObject(), getParentRoom().getUserByName(player.getUsername()));
	}

	public void getBid(String _username, float _value) {
		for (Player player : table.players) {
			if (player.isBusted)
				continue;
			if (player.getUsername().compareTo(_username) == 0) {
				player.bid = _value;
				table.rcvCount++;
			}
		}
		if (table.rcvCount == table.playerNum()) {
			table.delayFlag = false;
			synchronized (table.monitor) {
				table.monitor.notifyAll();
			}
		}
	}

	public void setBanker(Player player, float _value) {
		ISFSObject obj = new SFSObject();
		obj.putUtfString("username", player.getUsername());
		obj.putFloat("value", _value);
		if (getParentRoom().getUserList() != null)
			send("setBanker", obj, getParentRoom().getUserList());
	}

	public void dealSecondCards(boolean isNew) {
		for (Player player : table.players) {
			if (player.isBusted)
				continue;
			dealSecondCards(player, isNew);
		}
	}

	public void dealSecondCards(Player player, boolean isNew) {
		ISFSObject obj = new SFSObject();
		obj.putUtfString("username", player.getUsername());
		obj.putBool("isNew", isNew);
		obj.putBool("isArrange", player.isArrange);
		Card[] cards = player.getCards();
		for (int i = 0; i < 5; i++)
			obj.putInt("card" + i, cards[i].hashCode());
		if (getParentRoom().getUserList() != null)
			send("dealSecondCards", obj, getParentRoom().getUserList());
	}

	public void getArrange(String _username, int _value) {
		for (Player player : table.players) {
			if (player.isBusted)
				continue;
			if (player.getUsername().compareTo(_username) == 0) {
				player.arrangeCode = _value;
				player.isArrange = true;
				table.rcvCount++;
				hideArrangeCards(player);
			}
		}
		if (table.rcvCount == table.playerNum()) {
			table.delayFlag = false;
			synchronized (table.monitor) {
				table.monitor.notifyAll();
			}
		}
	}

	public void autoArrange(String _username) {
		for (Player player : table.players) {
			if (player.isBusted)
				continue;
			if (player.getUsername().compareTo(_username) == 0) {
				player.autoArrangeCards();
				ISFSObject obj = new SFSObject();
				obj.putInt("arrangeCode", player.arrangeCode);
				if (getParentRoom().getUserByName(player.getUsername()) != null)
					send("autoArrange", obj, getParentRoom().getUserByName(player.getUsername()));
			}
		}
	}

	public void hideArrangeCards(Player player) {
		if (getParentRoom().getUserByName(player.getUsername()) != null)
			send("hideArrangeCards", new SFSObject(), getParentRoom().getUserByName(player.getUsername()));
	}

	public void showSpecialWords(Player player) {
		ISFSObject obj = new SFSObject();
		obj.putInt("type", player.handValue.getType().getValue());
		Card[] cards = player.getCards();
		for (int i = 0; i < 5; i++)
			obj.putInt("card" + i, cards[i].hashCode());
		if (getParentRoom().getUserList() != null)
			send("showSpecialWords", obj, getParentRoom().getUserList());
	}

	public void showCards(Player player) {
		ISFSObject obj = new SFSObject();
		obj.putUtfString("username", player.getUsername());
		Card[] cards = player.getCards();
		for (int i = 0; i < 5; i++)
			obj.putInt("card" + i, cards[i].hashCode());
		if (getParentRoom().getUserList() != null)
			send("showCards", obj, getParentRoom().getUserList());
	}

	public void showWords(Player player) {
		ISFSObject obj = new SFSObject();
		obj.putUtfString("username", player.getUsername());
		obj.putInt("type", player.handValue.getType().getValue());
		obj.putInt("value", player.handValue.getValue());
		if (getParentRoom().getUserList() != null)
			send("showWords", obj, getParentRoom().getUserList());
	}

	public void showPayout(Player player, int amount) {
		ISFSObject obj = new SFSObject();
		obj.putUtfString("username", player.getUsername());
		obj.putInt("amount", amount);
		if (getParentRoom().getUserList() != null)
			send("showPayout", obj, getParentRoom().getUserList());
	}

	public void showResult(Player player, boolean isWin) {
		ISFSObject obj = new SFSObject();
		obj.putBool("isWin", isWin);
		if (getParentRoom().getUserByName(player.getUsername()) != null)
			send("showResult", obj, getParentRoom().getUserByName(player.getUsername()));
	}

	public void hideCards() {
		if (getParentRoom().getUserList() != null)
			send("hideCards", new SFSObject(), getParentRoom().getUserList());
	}

	public void showNewPlayer() {
		System.out.println("ShowNewPlayer");

		if (table.state != 0) {
			setTimer();
		}

		if (table.state == 1) {
			dealFirstCards(true);
		} else if (table.state == 2) {
			dealFirstCards(true);
			setBanker(table.banker, table.bid);
		} else if (table.state == 3) {
			setBanker(table.banker, table.bid);
			dealSecondCards(true);
		} else if (table.state == 4) {
			setBanker(table.banker, table.bid);
			dealSecondCards(true);
			for (Player player1 : table.players) {
				if (player1.isBusted)
					continue;
				showCards(player1);
				showWords(player1);
				if (table.activePlayer == player1)
					break;
			}
		} else if (table.state == 5) {
			setBanker(table.banker, table.bid);
			dealSecondCards(true);

			for (Player player1 : table.players) {
				if (player1.isBusted)
					continue;
				showCards(player1);
				showWords(player1);
			}

			int cash = 0;
			for (Player player1 : table.players) {
				if (player1.isBusted)
					continue;
				if (player1 != table.banker) {
					if (player1.handValue.compareTo(table.banker.handValue) == 1) {
						showPayout(player1, (int) (table.blind * table.bid * player1.handValue.getType().getValue()));
						cash -= (int) (table.blind * table.bid * player1.handValue.getType().getValue());
					} else if (player1.handValue.compareTo(table.banker.handValue) == -1) {
						showPayout(player1, 0 - (int) (table.blind * table.bid));
						cash += (int) (table.blind * table.bid);
					} else {
						showPayout(player1, 0);
					}
				}
			}
			showPayout(table.banker, cash);

		}
	}

	public ISFSObject getRoomData(String roomName) {
		IDBManager dbManager = getParentZone().getDBManager();
		String sql = "SELECT * FROM roomlist WHERE room_id=" + roomName;

		try {
			ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
			return res.getSFSObject(0);
		} catch (SQLException e) {
			trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
		}
		return null;
	}

	public int getCash(String username) {
		IDBManager dbManager = getParentZone().getDBManager();
		String sql = "SELECT chip FROM users WHERE username=\"" + username + "\"";

		try {
			ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
			return res.getSFSObject(0).getInt("chip");
		} catch (SQLException e) {
			trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
		}
		return 0;
	}

	public void payCash(String username, int amount) {
		int chip = getCash(username);

		IDBManager dbManager = getParentZone().getDBManager();
		String sql = "UPDATE users SET chip=" + (chip - amount) + " WHERE username=\"" + username + "\"";

		try {
			dbManager.executeUpdate(sql, new Object[] {});
		} catch (SQLException e) {
			trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
		}
	}

	public void addCash(String username, int amount) {
		int chip = getCash(username);

		IDBManager dbManager = getParentZone().getDBManager();
		String sql = "UPDATE users SET chip=" + (chip + amount) + " WHERE username=\"" + username + "\"";

		try {
			dbManager.executeUpdate(sql, new Object[] {});
		} catch (SQLException e) {
			trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
		}
	}

	public void addTopup(String username, int amount) {
		IDBManager dbManager = getParentZone().getDBManager();
		String sql = "INSERT INTO topup(username, table_name, score, datetime)" + " VALUES(\"" + username + "\",\""
				+ tableName + "\"," + amount + "," + (int) (System.currentTimeMillis() / 1000) + ")";

		try {
			dbManager.executeUpdate(sql, new Object[] {});
		} catch (SQLException e) {
			trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
		}
	}

}
