package RoomExtension;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class ArrangeHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		RoomExtension gameExt = (RoomExtension) getParentExtension();

		if (user.isPlayer())
		{
			if(params.getInt("param") == 0)
				gameExt.getArrange(user.getName(), params.getInt("value"));
			else if(params.getInt("param") == 1)
				gameExt.autoArrange(user.getName());
		}
		
	}
}
