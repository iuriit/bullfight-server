package ZoneExtension;

import java.sql.SQLException;
import java.util.List;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.BannedUser;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.managers.IBannedUserManager;
import com.smartfoxserver.v2.entities.managers.SFSBannedUserManager;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

import RoomExtension.RoomExtension;

public class GetBanListHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
        IBannedUserManager banManager = SmartFoxServer.getInstance().getBannedUserManager();
        List<BannedUser> array = (params.getInt("mode") == 0) ? banManager.getBannedUsersByName("BullFight") : banManager.getBannedUsersByIp();
        ISFSArray res = new SFSArray();
        for(BannedUser bUser : array) {
        	ISFSObject obj = new SFSObject();
        	if(params.getInt("mode") == 0)
        		obj.putUtfString("name", bUser.getName());
        	else
        		obj.putUtfString("ip", bUser.getIpAddress());
        	obj.putLong("banTime", bUser.getBanTimeMillis());
        	obj.putInt("duration", bUser.getBanDurationMinutes());
        	res.addSFSObject(obj);
        }
        ISFSObject response = new SFSObject();
        response.putSFSArray("array", res);
        send("getBanList", response, user);
	}
}
