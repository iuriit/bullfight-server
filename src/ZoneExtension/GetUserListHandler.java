package ZoneExtension;

import java.sql.SQLException;
import java.util.List;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.managers.IBannedUserManager;
import com.smartfoxserver.v2.entities.managers.SFSBannedUserManager;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

import RoomExtension.RoomExtension;

public class GetUserListHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
		String sql = "SELECT * FROM users WHERE privilege=0";

        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});            
            IBannedUserManager banManager = SmartFoxServer.getInstance().getBannedUserManager();
            for (int i = 0; i < res.size(); i ++) {
            	ISFSObject obj = res.getSFSObject(i);
            	if(banManager.isNameBanned(obj.getUtfString("username"), "BullFight")) {
            		obj.putInt("status", 2);		//Ban
            	}
            	else if(getParentExtension().getParentZone().getUserManager().containsName(obj.getUtfString("username"))) {
            		obj.putInt("status", 1);		//Online
            		System.out.println(obj.getUtfString("username"));
            	}
            	else {
            		obj.putInt("status", 0);		//Offline
            	}
            }
            ISFSObject response = new SFSObject();
            response.putSFSArray("array", res);
            send("getUserList", response, user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
