package ZoneExtension;

import java.sql.SQLException;
import java.util.List;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

import RoomExtension.RoomExtension;

public class GetBotListHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
        ISFSArray res = new SFSArray();
        List<Room> roomList = getParentExtension().getParentZone().getRoomList();
        for(Room room : roomList)
        {
        	if(!room.isGame())
        		continue;
        	ISFSArray array = (ISFSArray) room.getExtension().handleInternalMessage("getBotList", null);
        	for(int i = 0; i < array.size(); i ++) {
        		res.addSFSObject(array.getSFSObject(i));
        	}
        }
        ISFSObject response = new SFSObject();
        response.putSFSArray("array", res);
        send("getBotList", response, user);
	}
}
