package ZoneExtension;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class LoginHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
		String sql = "SELECT * FROM users WHERE username='" + params.getUtfString("username") + "'";
		if(params.containsKey("admin"))
			sql += " AND privilege<>0";
          
        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            ISFSObject response = new SFSObject();
            
            if(res.size() == 0) {
            	response.putBool("success", false);
            	response.putUtfString("reason", "username");
            }
            else {
                ISFSObject obj = (ISFSObject)(res.getElementAt(0));
                String pwd = obj.getUtfString("password");
                if(pwd.compareTo(params.getUtfString("password")) == 0) {
                	response.putBool("success", true);
                }
                else {
                	response.putBool("success", false);
                	response.putUtfString("reason", "password");
                }
            }
                          
            // Send back to requester
            send("login", response, user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
