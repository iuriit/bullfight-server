package ZoneExtension;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class RegisterHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
		String sql = "SELECT * FROM users WHERE username='" + params.getUtfString("username") + "'";
          
        try
        {
            // Obtain a resultset
            ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
            ISFSObject response = new SFSObject();
            
            if(res.size() > 0) {
            	response.putBool("success", false);
            	response.putUtfString("reason", "username");
            }
            else {
            	sql = "INSERT INTO users(username, password, reference, mobilephone, chip)"
            			+ " VALUES ('"
            			+ params.getUtfString("username") + "','"
            			+ params.getUtfString("password") + "','"
            			+ params.getUtfString("reference") + "','"
            			+ params.getUtfString("mobilephone") + "','"
            			+ 10000 + "')";
                try {
                    dbManager.executeUpdate(sql, new Object[] {});
                    response.putBool("success", true);
                }
                catch (SQLException e) {
                    trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
                }
            }
                          
            // Send back to requester
            send("register", response, user);
        }
        catch (SQLException e)
        {
            trace(ExtensionLogLevel.WARN, "SQL Failed: " + e.toString());
        }
	}
}
