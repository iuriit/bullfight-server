package ZoneExtension;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.CreateRoomSettings.RoomExtensionSettings;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

import RoomExtension.RoomExtension;

public class AddRoomHandler extends BaseClientRequestHandler
{
	
	private static final String EXTENSION_ID = "BullFight";
	private static final String EXTENSION_CLASS = "RoomExtension.RoomExtension";

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		ISFSObject response = new SFSObject();
		Room room = getParentExtension().getParentZone().getRoomByName(params.getUtfString("room_id"));
		if(room != null)
		{
			response.putBool("success", false);
	        send("addRoom", response, user);
		}
		
        int i = 0;
		CreateRoomSettings settings = new CreateRoomSettings();
		settings.setName(params.getUtfString("room_id"));
		settings.setGroupId("Game");
		settings.setGame(true);
		settings.setExtension(new RoomExtensionSettings(EXTENSION_ID, EXTENSION_CLASS));
		settings.setAutoRemoveMode(SFSRoomRemoveMode.NEVER_REMOVE);
		
		List<RoomVariable> roomVars = new ArrayList<RoomVariable>(); 
		roomVars.add(new SFSRoomVariable("param", params));
		settings.setRoomVariables(roomVars);
		
		try {
			getApi().createRoom(getParentExtension().getParentZone(), settings, null);
		} catch (SFSCreateRoomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.putBool("success", true);
        send("addRoom", response, user);
	}
}
