package ZoneExtension;

import java.sql.SQLException;
import java.util.List;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

import RoomExtension.RoomExtension;

public class GetRoomListHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
        int type = params.getInt("type");
        ISFSArray res = new SFSArray();
        List<Room> roomList = getParentExtension().getParentZone().getRoomList();
        for(Room room : roomList)
        {
        	if(!room.isGame())
        		continue;
        	ISFSObject obj = (ISFSObject) room.getExtension().handleInternalMessage("getRoomInfo", null);
        	if(type == 0 && obj.getInt("blind") < 20)
        		res.addSFSObject(obj);
        	if(type == 1 && obj.getInt("blind") >= 20 && obj.getInt("blind") < 100)
        		res.addSFSObject(obj);
        	if(type == 2 && obj.getInt("blind") >= 100)
        		res.addSFSObject(obj);
        	if(type == 3)
        		res.addSFSObject(obj);
        }
        ISFSObject response = new SFSObject();
        response.putSFSArray("array", res);
        send("getRoomList", response, user);
	}
}
