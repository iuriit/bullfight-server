package ZoneExtension;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.managers.BanMode;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class PlayerActionHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		User player = getParentExtension().getParentZone().getUserByName(params.getUtfString("name"));
		if(player != null) {
			if (params.getInt("action") == 1) {				// Kick
				getApi().kickUser(player, user, "kick", params.getInt("delay"));
			}
			else if (params.getInt("action") == 2) {		// Ban
				getApi().banUser(player, user, "ban", (params.getInt("mode") == 0) ? BanMode.BY_NAME : BanMode.BY_ADDRESS, params.getInt("duration"), params.getInt("delay"));
			}
		}
	}
}
