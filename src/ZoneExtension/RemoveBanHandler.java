package ZoneExtension;

import java.sql.SQLException;
import java.util.List;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.BannedUser;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.managers.BanMode;
import com.smartfoxserver.v2.entities.managers.IBannedUserManager;
import com.smartfoxserver.v2.entities.managers.SFSBannedUserManager;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

import RoomExtension.RoomExtension;

public class RemoveBanHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
        IBannedUserManager banManager = SmartFoxServer.getInstance().getBannedUserManager();
        if(params.getInt("mode") == 0)
        	banManager.removeBannedUser(params.getUtfString("name"), "BullFight", BanMode.BY_NAME);
        else
        	banManager.removeBannedUser(params.getUtfString("name"), null, BanMode.BY_ADDRESS);
        send("removeBan", new SFSObject(), user);
	}
}
