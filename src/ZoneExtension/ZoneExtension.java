package ZoneExtension;

import com.smartfoxserver.v2.extensions.SFSExtension;

public class ZoneExtension extends SFSExtension {
	
	@Override
	public void init() {
		addRequestHandler("login", LoginHandler.class);
		addRequestHandler("register", RegisterHandler.class);
		addRequestHandler("worldMessage", WorldMessageHandler.class);
		addRequestHandler("getUserInfo", GetUserInfoHandler.class);
		addRequestHandler("getUserList", GetUserListHandler.class);
		addRequestHandler("getRoomList", GetRoomListHandler.class);
		addRequestHandler("addRoom", AddRoomHandler.class);
		addRequestHandler("removeRoom", RemoveRoomHandler.class);
		addRequestHandler("banUser", BanUserHandler.class);
		addRequestHandler("kickUser", KickUserHandler.class);
		addRequestHandler("getBanList", GetBanListHandler.class);
		addRequestHandler("removeBan", RemoveBanHandler.class);
		addRequestHandler("getBotList", GetBotListHandler.class);
		addRequestHandler("addBot", AddBotHandler.class);
		addRequestHandler("removeBot", RemoveBotHandler.class);
		addRequestHandler("getTopupList", GetTopupListHandler.class);
	}
	
	@Override
	public void destroy() {
		super.destroy();
	}
	
}
