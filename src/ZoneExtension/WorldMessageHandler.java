package ZoneExtension;

import java.util.List;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class WorldMessageHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		List<User> userList = (List<User>)getParentExtension().getParentZone().getUserList();
		send("worldMessage", params, userList);
	}
}
