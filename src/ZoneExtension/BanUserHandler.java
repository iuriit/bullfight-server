package ZoneExtension;

import java.sql.SQLException;
import java.util.List;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.managers.BanMode;
import com.smartfoxserver.v2.entities.managers.IBannedUserManager;
import com.smartfoxserver.v2.entities.managers.SFSBannedUserManager;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

import RoomExtension.RoomExtension;

public class BanUserHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
        IBannedUserManager banManager = SmartFoxServer.getInstance().getBannedUserManager();
		User userToBan = getParentExtension().getParentZone().getUserManager().getUserByName(params.getUtfString("username"));
        if(params.getInt("mode") == 0)
	        banManager.banUser(params.getUtfString("username"), "BullFight", params.getInt("duration"), BanMode.BY_NAME, "");
        else
        {
        	if(userToBan != null)
        		getApi().banUser(userToBan, null, "", BanMode.BY_ADDRESS, params.getInt("duration"), 0);
        }
		if(userToBan != null)
			getApi().kickUser(userToBan, user, "", 0);
        send("banUser", new SFSObject(), user);
	}
}
