
package PokerEngine;

public class HandEvaluator {
        
    /** The hand value type. */
    private HandValueType type;
    
    /** The hand value as integer number. */
    private int value = 0;
    
    /** The cards. */
    private final Card[] cards;
    
    private int maxVal = -1;
    
    /**
     * Constructor.
     *
     * @param  hand  The hand to evaluate.
     */
    public HandEvaluator(Hand hand) {
        cards = hand.getCards();
        
        if(isFiveDukes()) {
        	type = HandValueType.FIVE_DUKES;
        	value = 0;
        }
        else if(isBullOfSpades()) {
        	type = HandValueType.BULL_OF_SPADES;
        	value = 0;
        }
        else if(isBull()) {
        	if(isPairOfAces()) {
            	type = HandValueType.PAIR_OF_ACES;
            	value = 0;
        	}
        	else if(isPair()) {
            	type = HandValueType.BULL_PAIR;
            	value = cards[3].getRank();
        	}
        	else if(isBigBull()) {
            	type = HandValueType.BIG_BULL;
            	value = 0;
        	}
        	else {
            	type = HandValueType.BULL;
            	value = maxVal;
        	}
        }
        else {
        	type = HandValueType.NO_BULL;
        	value = 0;
        }
    }
    
    /**
     * Returns the hand value type.
     *
     * @return  the hand value type
     */
    public HandValueType getType() {
        return type;
    }
    
    /**
     * Returns the hand value as an integer.
     * 
     * This method should be used to compare hands.
     *
     * @return  the hand value
     */
    public int getValue() {
        return value;
    }    
    
    private boolean isFiveDukes() {
    	for(int i = 0; i < 5; i ++) {
    		if(cards[i].getRank() < Card.JACK || cards[i].getRank() > Card.KING)
    			return false;
    	}
    	return true;
    }

    private boolean isBullOfSpades() {
    	int aceCount = 0, dukeCount = 0;
    	for(int i = 0; i < 5; i ++) {
    		if(cards[i].getRank() == Card.ACE)
    			aceCount ++;
    		else if(cards[i].getRank() >= Card.JACK && cards[i].getRank() <= Card.KING)
    			dukeCount ++;
    	}
    	if(aceCount == 1 && dukeCount == 4)
    		return true;
    	return false;
    }
    
    private boolean isBull() {
    	maxVal = 0;
    	getMaxValue(0, 0);
    	return (maxVal == 10);
    }
    
    private boolean isPairOfAces() {
    	return (cards[3].getRank() == Card.ACE && cards[4].getRank() == Card.ACE);
    }

    private boolean isPair() {
    	return (cards[3].getRank() == cards[4].getRank());
    }

    private boolean isBigBull() {
    	maxVal = 0;
    	getMaxValue(3, -1);
    	return (maxVal == 10);
    }
    
    private void getMaxValue(int i, int v) {
    	if((i == 3 && v != -1)|| i == 5) {
    		v = (v % 10 == 0) ? 10 : v % 10;
    		if(maxVal < v)
    			maxVal = v;
    		return;
    	}
    	if(v == -1)
    		v = 0;
    	if(cards[i].getRank() == Card.THREE || cards[i].getRank() == Card.SIX) {
    		getMaxValue(i + 1, v + 3);
    		getMaxValue(i + 1, v + 6);
    	}
    	else
    		getMaxValue(i + 1, v + getCardValue(cards[i].getRank()));
    }
        
    private int getCardValue(int _rank) {
    	if(_rank == Card.ACE)
    		return 1;
    	if(_rank >= Card.JACK && _rank <= Card.KING)
    		return 10;
    	return _rank + 2;
    }
}
