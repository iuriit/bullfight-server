// This file is part of the 'texasholdem' project, an open source
// Texas Hold'em poker application written in Java.
//
// Copyright 2009 Oscar Stigter
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package PokerEngine;

import java.util.Collection;

public class Hand {
    
    private static final int MAX_NO_OF_CARDS = 5;
    
    private Card[] cards = new Card[MAX_NO_OF_CARDS];
    
    private int noOfCards = 0;
    
    public Hand() {
        // Empty implementation.
    }
    
    public Hand(Card[] cards) {
        addCards(cards);
    }
    
    public Hand(Collection<Card> cards) {
        if (cards == null) {
            throw new IllegalArgumentException("Null array");
        }
        for (Card card : cards) {
            addCard(card);
        }
    }
    
    public Hand(String s) {
        if (s == null || s.length() == 0) {
            throw new IllegalArgumentException("Null or empty string");
        }
        
        String[] parts = s.split("\\s");
        if (parts.length > MAX_NO_OF_CARDS) {
            throw new IllegalArgumentException("Too many cards in hand");
        }
        for (String part : parts) {
            addCard(new Card(part));
        }
    }
    
    public int size() {
        return noOfCards;
    }
    
    public void addCard(Card card) {
        if (card == null) {
            throw new IllegalArgumentException("Null card");
        }        
        cards[noOfCards++] = card;
    }
    
    public void addCards(Card[] cards) {
        if (cards == null) {
            throw new IllegalArgumentException("Null array");
        }
        if (cards.length > MAX_NO_OF_CARDS) {
            throw new IllegalArgumentException("Too many cards");
        }
        for (Card card : cards) {
            addCard(card);
        }
    }
    
    public void addCards(Collection<Card> cards) {
        if (cards == null) {
            throw new IllegalArgumentException("Null collection");
        }
        if (cards.size() > MAX_NO_OF_CARDS) {
            throw new IllegalArgumentException("Too many cards");
        }
        for (Card card : cards) {
            addCard(card);
        }
    }
    
    public Card[] getCards() {
        Card[] dest = new Card[noOfCards];
        System.arraycopy(cards, 0, dest, 0, noOfCards);
        return dest;
    }
    
    public void removeAllCards() {
        noOfCards = 0;
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < noOfCards; i++) {
            sb.append(cards[i]);
            if (i < (noOfCards - 1)) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }
    
    public void arrangeCards(int arrangeCode)
    {
    	int sq;
    	Card[] tmp = new Card[5];
    	for(int i = 0; i < 5; i ++)
    		tmp[i] = cards[i];
    	for(int i = 4; i >= 0; i --) {
    		sq = arrangeCode % 5;
    		arrangeCode /= 5;
    		cards[sq] = tmp[i];
    	}
    }
    
}
