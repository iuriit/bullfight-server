package PokerEngine;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import RoomExtension.RoomExtension;

public class Table {
    private final Deck deck;    
    private final RoomExtension gameExt;
    public List<Player> players;
    public List<Player> newPlayers;
    public List<Player> exitPlayers;
    public boolean isRunning;
    public int blind;
    public float bid = 0;
    public int rcvCount;
    public Player banker;
    public Player activePlayer;
    public int state = 0;
    public long startTime;

    private Timer timer;
    public boolean delayFlag;
    public Object monitor = new Object();

    public Table(int _blind, RoomExtension _ext) {
    	this.blind = _blind;
        this.gameExt = _ext;
        this.isRunning = false;
        players = new ArrayList<>();
        newPlayers = new ArrayList<>();
        exitPlayers = new ArrayList<>();
        deck = new Deck();
        timer = new Timer();
    }
    
    public void addPlayer(Player player) {
        newPlayers.add(player);
    }
    
    public boolean isRunning() {
    	return isRunning;
    }
    
    public int playerSize() {
    	return players.size() + newPlayers.size();
    }
    
    public int playerNum() {
    	int n = 0;
    	for(Player player : players) {
    		if(!player.isBot() && !player.isBusted) {
    			n ++;
    		}
    	}
    	return n;
    }
    
    public void run()
    {
    	isRunning = true;
        while (true) {
        	for (Player player : newPlayers) {
        		players.add(player);
        	}
        	newPlayers.clear();
        	
        	for (Player player : exitPlayers) {
    			players.remove(player);
    			gameExt.removePlayer(player);
        	}
        	exitPlayers.clear();
        	
        	gameExt.showPlayers();
        	
            int noOfActivePlayers = 0;
            for (Player player : players) {
                if (player.getCash() >= blind) {
                    noOfActivePlayers++;
                }
                else {
                	player.isBusted = true;
                }
            }
            if (noOfActivePlayers > 1) {
                playHand();
            } else {
                break;
            }
        }
        
        isRunning = false;
        
        delayTimer(2f);
    	for (Player player : players) {
    		if(player.isBusted) {
    			players.remove(player);
    			gameExt.removePlayer(player);
    		}
    	}
        
        // Game over.
    }
    
    private void playHand()
    {
    	resetHand();

    	delayTimer(1);
    	dealFirstCards();

    	setBanker();
    	
    	delayTimer(1);
    	dealSecondCards();

    	arrangeCards();
    	
    	showCards();
    	
    	delayTimer(1);
    	doPayout();
    	
    	delayTimer(2);
    	state = 0;
    	gameExt.hideCards();
    }
    
    private void resetHand() {
        for (Player player : players) {
            player.resetHand();
        }
        
        // Shuffle the deck.
        deck.shuffle();
    	state = 7;
    	startTime = System.currentTimeMillis();
    	gameExt.setTimer();
    }
    
    private void dealFirstCards()
    {
    	for(Player player : players) {
    		if(!player.isBusted)
    			player.addCards(deck.deal(3));
    	}
    	gameExt.dealFirstCards(false);
    	state = 1;
    }
    
    private void setBanker()
    {
    	Random rand = new Random();
    	rcvCount = 0;
    	for(Player player : players) {
    		if(player.isBusted)
    			continue;
    		if(player.isBot()) {
    			int r = rand.nextInt(3);
    			if(r == 0)
    				player.bid = 0f;
    			else if(r == 1)
    				player.bid = 1f;
    			else if(r == 2)
    				player.bid = 1.5f;
    			else
    				player.bid = 2f;
    		}
    	}
    	delayTimer(4);

    	int num = 0;
    	bid = 0;
    	for(Player player : players) {
    		if(player.isBusted)
    			continue;
    		System.out.println(player.getName() + "," + player.bid);
    		if(player.bid == bid) {
    			num ++;
    		}
    		else if(player.bid > bid) {
    			bid = player.bid;
    			num = 1;
    		}
    	}
    	
    	if(num > 1)
    		num = rand.nextInt(num - 1);
    	else
    		num = 0;

    	for(Player player : players) {
    		if(player.isBusted)
    			continue;
    		if(player.bid == bid) {
    			if(num == 0)
    				banker = player;
    			num --;
    		}
    	}
    	
    	if(bid == 0)
    		bid = 1;
    	gameExt.setBanker(banker, bid);
    	state = 2;
    }

    private void dealSecondCards()
    {
    	for(Player player : players) {
    		if(player.isBusted)
    			continue;
    		player.addCards(deck.deal(2));
    	}
    	gameExt.dealSecondCards(false);
    	state = 3;
    }
    
    public void switchCard(Player player)
    {
    	player.resetHand();
    	player.addCards(deck.deal(5));
    }
    
    private void arrangeCards()
    {
    	rcvCount = 0;
    	delayTimer(20);
    	System.out.println("Arrange Cards");
    	for(Player player : players) {
    		if(player.isBusted)
    			continue;
    		player.isArrange = true;
    		player.autoArrangeCards();
    		player.arrangeCards();
    		player.handValue.calculate(player.hand);
    		
    		if(player.isBot()) {
    			double rand = Math.random();
    			if((player.level == 1 && rand < 0.3f) || (player.level == 2 && rand < 0.6f)) {
    				Player newPlayer = new Player("", "", 0, true);
    				List<Card> cardList = deck.deal(5);
    				newPlayer.addCards(cardList);
    				newPlayer.autoArrangeCards();
    				newPlayer.arrangeCards();
    				newPlayer.handValue.calculate(newPlayer.hand);
    				if(newPlayer.handValue.compareTo(player.handValue) == 1) {
    					player.resetHand();
    					player.addCards(cardList);
    		    		player.isArrange = true;
    		    		player.autoArrangeCards();
    		    		player.arrangeCards();
    		    		player.handValue.calculate(player.hand);
    				}
    			}
    		}
    		
    		gameExt.hideArrangeCards(player);
    	}
    }

    private void showCards()
    {
    	state = 4;
    	activePlayer = null;
    	for(Player player : players) {
    		if(player.isBusted)
    			continue;
    		delayTimer(1);
    		if(player.handValue.getType().getValue() > 1) {
    			gameExt.showSpecialWords(player);
    			delayTimer(5);
    		}
    		gameExt.showCards(player);
    		gameExt.showWords(player);
    		activePlayer = player;
    	}
    }
    
    private void doPayout()
    {
    	int cash = 0;
    	for(Player player : players) {
    		if(player.isBusted)
    			continue;
    		if(player != banker) {
    			if(player.handValue.compareTo(banker.handValue) == 1) {
    				gameExt.showPayout(player, (int)(blind * bid * player.handValue.getType().getValue()));
    				cash -= (int)(blind * bid * player.handValue.getType().getValue());
    			}
    			else if(player.handValue.compareTo(banker.handValue) == -1) {
    				gameExt.showPayout(player, 0 - (int)(blind * bid));
    				cash += (int)(blind * bid);
    			}
    			else {
    				gameExt.showPayout(player, 0);
    			}
    		}
    	}
    	gameExt.showPayout(banker, cash);
    	state = 5;
    	
    	delayTimer(2);
    	for(Player player : players) {
    		if(player.isBusted)
    			continue;
    		if(player != banker) {
    			if(player.handValue.compareTo(banker.handValue) == 1) {
    				gameExt.showResult(player, true);
    			}
    			else if(player.handValue.compareTo(banker.handValue) == -1) {
    				gameExt.showResult(player, false);
    			}
    		}
    	}
    	if(cash > 0)
			gameExt.showResult(banker, true);
    	else if(cash < 0)
			gameExt.showResult(banker, false);

    	delayTimer(5);
    	for(Player player : players) {
    		if(player.isBusted)
    			continue;
    		if(player != banker) {
    			if(player.handValue.compareTo(banker.handValue) == 1) {
    				player.win((int)(blind * bid * player.handValue.getType().getValue()));
    				if(!player.isBot())
    					gameExt.addTopup(player.getUsername(), (int)(blind * bid * player.handValue.getType().getValue()));
    			}
    			else if(player.handValue.compareTo(banker.handValue) == -1) {
    				player.payCash((int)(blind * bid));
    				if(!player.isBot())
    					gameExt.addTopup(player.getUsername(), -(int)(blind * bid));
    			}
    		}
    	}
    	banker.win(cash);
    	if(!banker.isBot())
    		gameExt.addTopup(banker.getUsername(), cash);
    	gameExt.showPlayers();
    }

    private void delayTimer(float _t)
    {
    	delayFlag = true;
		// SetTimer
    	timer = new Timer();
		timer.schedule(new TimerTask() {
			  @Override
			  public void run() {
			    // Your database code here
				  if(delayFlag) {
					  delayFlag = false;
					  synchronized (monitor) {
						  monitor.notifyAll();
					  }
				  }
			  }
			}, (long)(_t*1000));
		
		while(delayFlag) {
            // Wait for the user to select an action.
            synchronized (monitor) {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    // Ignore.
                }
            }
		}
        timer.cancel();
    }

}
