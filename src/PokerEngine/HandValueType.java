
package PokerEngine;

/**
 * The hand value types in poker.
 * 
 */
public enum HandValueType {

    FIVE_DUKES("Five Dukes", 8),
    BULL_OF_SPADES("Bull of Spades", 5),
    PAIR_OF_ACES("Pair of Aces", 4),
    BULL_PAIR("Bull Pair", 3),
    BIG_BULL("Big Bull", 2),
    BULL("Bull", 1),
    NO_BULL("No Bull", 0),
    ;
    
    /** The description. */
    private String description;

    /** The hand value. */
    private int value;
    
    /**
     * Constructor.
     * 
     * @param description
     *            The description.
     * @param value
     *            The hand value.
     */
    HandValueType(String description, int value) {
        this.description = description;
        this.value = value;
    }
    
    /**
     * Returns the description.
     * 
     * @return The description.
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * Returns the hand value.
     * 
     * @return The hand value.
     */
    public int getValue() {
        return value;
    }
    
}
