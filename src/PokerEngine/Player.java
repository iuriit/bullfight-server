// This file is part of the 'texasholdem' project, an open source
// Texas Hold'em poker application written in Java.
//
// Copyright 2009 Oscar Stigter
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package PokerEngine;

import java.util.List;

public class Player {
    
    private final String username;
    private final String name;
    private final boolean isBot;
    public int level = 0;
    public boolean isBusted;
    public final Hand hand;
    private int cash;
    private boolean hasCards;
    public float bid;
    public int arrangeCode;
	public boolean isArrange;
	public boolean isExit;
    public HandValue handValue;
	private boolean[] flag = new boolean[5];
	private int autoArrangeCode;

    public Player(String _username, String _name, int _cash, boolean _isBot) {
    	this.username = _username;
        this.name = _name;
        this.cash = _cash;
        this.isBot = _isBot;
        this.isBusted = false;
        this.isExit = false;

        hand = new Hand();
        handValue = new HandValue();

        resetHand();
    }
    
    /**
     * Prepares the player for another hand.
     */
    public void resetHand() {
    	arrangeCode = arrangeEncode(0, 1, 2, 3, 4);
    	bid = 0f;
    	hasCards = false;
    	isArrange = false;
        hand.removeAllCards();
    }

    public void addCards(List<Card> cards) {
        if (cards != null) {
            hand.addCards(cards);
            hasCards = true;
            System.out.format("[CHEAT] %s's cards:\t%s\n", name, hand);
        }
    }

    public Card[] getCards() {
        return hand.getCards();
    }

    public boolean hasCards() {
        return hasCards;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public boolean isBot() {
    	return isBot;
    }

    public int getCash() {
        return cash;
    }

    public void payCash(int amount) {
        if (amount > cash) {
            throw new IllegalStateException("Player asked to pay more cash than he owns!");
        }
        cash -= amount;
    }
    
    public void win(int amount) {
    	cash += amount;
    }
    
    public Player publicClone() {
        Player clone = new Player(username, name, cash, isBot);
        clone.hasCards = hasCards;
        return clone;
    }
    
    public int arrangeEncode(int s1, int s2, int s3, int s4, int s5)
    {
    	return ((((s1 * 5) + s2) * 5 + s3) * 5 + s4) * 5 + s5;
    }
    
    public void arrangeCards()
    {
    	hand.arrangeCards(arrangeCode);
    }
    
    public void autoArrangeCards()
    {
    	int[] arr = {0, 1, 2, 3, 4};
    	printCombination(arr, 5, 3);
    }
     
    void printCombination(int arr[], int n, int r)
    {
        int[] data = new int[5];
        combinationUtil(arr, data, 0, n-1, 0, r);
    }
    
    int tmp[] = new int[5];
     
    void combinationUtil(int arr[], int data[], int start, int end,
                         int index, int r)
    {
        // Current combination is ready to be printed, print it
        if (index == r)
        {
        	for(int j = 0; j < 5; j ++)
        		flag[j] = true;
            for (int j=0; j<r; j++)
            	flag[data[j]] = false;
            for (int j = 0; j < 5; j ++) {
            	if(flag[j])
            		data[r ++] = j;
            }
            for (int j = 0; j < 5; j ++) {
            	tmp[data[j]] = j;
            }
            int ac = arrangeEncode(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4]);
            Hand h = new Hand(hand.toString());
            h.arrangeCards(arrangeCode);
            HandValue hv = new HandValue();
            hv.calculate(h);
            Hand h1 = new Hand(hand.toString());
            h1.arrangeCards(ac);
            HandValue hv1 = new HandValue();
            hv1.calculate(h1);
//            System.out.println(h.toString() + " : " + h1.toString());
//            System.out.println(hv.getDescription() + " : " + hv1.getDescription());
            if(hv.compareTo(hv1) == -1)
            {
            	arrangeCode = ac;
            }
            return;
        }
     
        for (int i=start; i<=end && end-i+1 >= r-index; i++)
        {
            data[index] = arr[i];
            combinationUtil(arr, data, i+1, end, index+1, r);
        }
    }
}
